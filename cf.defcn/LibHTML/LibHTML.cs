﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cf.defcn
{
    public enum HTElements
    {
        HTML,
        ENDHTML,
        HEAD,
        ENDHEAD,
        BODY,
        ENDBODY,
        A,
        P,
        BR,
        DIV,
        VIDEO,
        AUDIO,
        STYLE,
        SCRIPT
    }
    public struct DOMElement
    {
        HTElements Type;
        string Name;
        string Class_;
        string Content;
        public DOMElement(HTElements type, string name, string Class, string content)
        {
            Type = type;
            Name = name;
            Class_ = Class;
            Content = content;
        }
        public override string ToString()
        {
            string line = string.Empty;
            switch (Type)
            {
                case HTElements.HTML:
                    line = "<html>";
                    return line;
                case HTElements.ENDHTML:
                    line = "</html>";
                    return line;
                //break;
                case HTElements.HEAD:
                    line = "<head>";
                    return line;
                case HTElements.ENDHEAD:
                    line = "</head>";
                    return line;
                case HTElements.BODY:
                    line = "<body>";
                    return line;
                case HTElements.ENDBODY:
                    line = "</body>";
                    return line;
                case HTElements.A:
                    line = string.Format("<a {0} {1}>{2}</a>", (Name.Length > 0) ? string.Format("name=\"{0}\"", Name) : "", (Class_.Length > 0) ? string.Format("class=\"{0}\"", Class_) : "", Content);
                    return line;
                case HTElements.P:
                    line = string.Format("<p {0} {1}>{2}</p>", (Name.Length > 0) ? string.Format("name=\"{0}\"", Name) : "", (Class_.Length > 0) ? string.Format("class=\"{0}\"", Class_) : "", Content);
                    break;
                case HTElements.BR:
                    break;
                case HTElements.DIV:
                    line = string.Format("<div {0} {1}>{2}</div>", (Name.Length > 0) ? string.Format("name=\"{0}\"", Name) : "", (Class_.Length > 0) ? string.Format("class=\"{0}\"", Class_) : "", Content);
                    break;
                case HTElements.VIDEO:
                    break;
                case HTElements.AUDIO:
                    break;
                case HTElements.STYLE:
                    throw new NotImplementedException("HTElements.STYLE@.ToString() is not implemented@~79");
                //break;
                case HTElements.SCRIPT:
                    throw new NotImplementedException("HTElements.SCRIPT@.ToString() is not implemented@~82");
                //break;
                default:
                    return string.Format("<!--Unknow Data: {0} -->", Content);
                    //break;
            }
            return line;
        }
    }
    public struct HTMLDOC
    {
        const string Header = "<!doctype html>";
        List<HTElements> HTData;

        public string[] ToStringArray()
        {

            List<string> a = new List<string>();
            a.Add(Header);
            foreach (var v in HTData)
            {
                a.Add(v.ToString());
            }
            return a.ToArray();
        }

    }
    /// <summary>
    /// Helper class for Generating HTML Document.
    /// Not fully implemented, may be buggy.
    /// </summary>
    class LibHTML
    {
        
    }
}
