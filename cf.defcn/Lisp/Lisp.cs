﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cf.defcn.Lisp
{
  public  class a
    {
        public class SNode
        {
            public String Name { get; set; }

            private readonly List<SNode> _Nodes = new List<SNode>();
            public ICollection<SNode> Nodes { get { return _Nodes; } }
        }

        public String Serialize(SNode root)
        {
            var sb = new StringBuilder();
            Serialize(root, sb);
            return sb.ToString();
        }

        private void Serialize(SNode node, StringBuilder sb)
        {
            sb.Append('(');

            sb.Append(node.Name);

            foreach (var item in node.Nodes)
                Serialize(item, sb);

            sb.Append(" )");
        }

        public SNode Deserialize(string st)
        {
            int pos;
            if (string.IsNullOrWhiteSpace(st))
                return null;

            var node = new SNode();

            var nodesPos = st.IndexOf('(');
            var endPos = st.LastIndexOf(')');

            var childrenString = st.Substring(nodesPos, endPos - nodesPos);

            node.Name = st.Substring(1, (nodesPos >= 0 ? nodesPos : endPos)).TrimEnd();

            var childStrings = new List<string>();

            int brackets = 0;
            int startPos = nodesPos;
            for ( pos = nodesPos;  pos < endPos;pos++)
            {
                if (st[pos] == '(')
                    brackets++;
                else if (st[pos] == ')')
                {
                    brackets--;

                    if (brackets == 0)
                    {
                        childStrings.Add(st.Substring(startPos, pos - startPos + 1));
                        startPos = pos + 1;
                    }
                }
                
            }

            foreach (var child in childStrings)
            {
                var childNode = Deserialize(child);
                if (childNode != null)
                    node.Nodes.Add(childNode);
            }

            return node;
        }
    }
   public class Lisp
   {
       public Lisp( )
       {

       }
        struct ParsedLine
        {
            string Cmd;
            string Params;
            public string Interpret( )
            {
                if (Cmd.Length==0|| Cmd==null)
                {
                    return "CMD NULL";
                }
                if (Params.Length==0||Params==null)
                {
#if DEBUG
                   Console.WriteLine(@"DEBUG:[NO PARAMETER], TO DISABLE THIS WARN, USE RELEASE BINARY");     
#endif
                }
                switch (Cmd)
                {
                    case "WriteL":
                        break;
                    case "Write":
                        break;
                    case "Mbox":
                        break;
                    case "GetLog":
                        break;
                    case "UploadPicture":
                        break;
                    case "CompressDir":
                        break;
                    case "InstallService":
                        break;
                    case "Help":
                        break;
                    case "Error":
                        break;
                    default:
#if DEBUG
                        Console.WriteLine("Unknow command");
#endif
                        break;
                }

                return "";
            }

        }


        public void aef(string eya)
        {
            int a=eya.IndexOf('(');
            int b=eya.IndexOf(')');

            Console.WriteLine(eya.Substring(a+1,b-1));
        }

    }
}
