﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cf.defcn;
using System.Net;
namespace cf.defcn
{
    class Program
    {
        static void Main(string[] args)
        {
            LuaWrapper lw = new LuaWrapper();
            var engine = lw.Create();
            engine["wc"] = new WebClient();
            //engine["Keylogger"] = new cf.defcn.Keylogger(true);
            while (true)
            {
                Console.Write(">");
                var a = Console.ReadLine();
                engine.DoString(a);
            }

        }
    }
}
