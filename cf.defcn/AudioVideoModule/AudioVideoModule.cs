﻿#pragma warning disable 0162
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Diagnostics;


namespace cf.defcn
{//ffmpeg -f gdigrab -framerate 6 -i desktop out.mpg -->grab desktop
 public   class FFMPEG
    {
#pragma warning disable CS0414
        readonly string FFBin = "ffmpeg.exe";
#pragma warning disable CS0162
        public enum Encoders
        {
            NULL = -1,
            apng,
            libxavs,
            gif,
            flv,
            h261,
            h263,
            h263p,
            libx264,
            libx264rgb,
            libx265,
            jpeg2000,
            mpeg1video,
            mpeg2video,
            mpeg4,
            libxvid,
            msmpeg4v2,
            msmpeg4,
            msvideo1,
            png,
            libtheora,
            libvpx,
            libvpx_vp9,
            libwebp,
            wmv1,
            wmv2,
            aac,
            ac3,
            ac3_fixed,
            alac,
            flac,
            mp2,
            mp2fixed,
            libmp3lame,
            libopus,
            vorbis,
            libvorbis,
            wavpack,
            libwavpack,
            wmav1,
            wmav2

        }
#pragma warning disable CS0162
        public enum InputProtocols
        {
            NULL = -1,
            async,
            bluray,
            cache,
            concat,
            crypto,
            data,
            file,
            ftp,
            gopher,
            hls,
            http,
            httpproxy,
            https,
            mmsh,
            mmst,
            pipe,
            rtp,
            srtp,
            subfile,
            tcp,
            tls,
            udp,
            udplite,
            rtmp,
            rtmpe,
            rtmps,
            rtmpt,
            rtmpte

        }
#pragma warning disable CS0162
        public enum OutputProtocols
        {
            NULL = -1,
            crypto,
            file,
            ftp,
            gopher,
            http,
            httpporxy,
            https,
            icecast,
            md5,
            pipe,
            rtp,
            srtp,
            tcp,
            tls,
            udp,
            udplite,
            rtmp,
            rtmpe,
            rtmps,
            rtmpt,
            rtmpte

        }
        public struct Command
        {
            Encoders e;
            InputProtocols inp;
            OutputProtocols outp;
            string infile;
            string outfile;
            public Command(Encoders VEncoder, InputProtocols Input, OutputProtocols Out, string Infile, string Outfile)
            {
                e = VEncoder;
                inp = Input;
                outp = Out;
                infile = Infile;
                outfile = Outfile;
            }
            void Start(Encoders e)
            {
                FFMPEG f = new FFMPEG();



            }
            string GetEncoder()
            {
                switch (e)
                {
                    case Encoders.NULL:
                        return "";
                        break;
                    case Encoders.apng:
                        return "apng";
                        break;
                    case Encoders.libxavs:
                        return "libxavs";
                        break;
                    case Encoders.gif:
                        return "gif";
                        break;
                    case Encoders.flv:
                        return "flv";
                        break;
                    case Encoders.h261:
                        return "h261";
                        break;
                    case Encoders.h263:
                        return "h263";
                        break;
                    case Encoders.h263p:
                        return "h263p";
                        break;
                    case Encoders.libx264:
                        return "libx264";
                        break;
                    case Encoders.libx264rgb:
                        return "libx264rgb";
                        break;
                    case Encoders.libx265:
                        return "libx265";
                        break;
                    case Encoders.jpeg2000:
                        return "jpeg2000";
                        break;
                    case Encoders.mpeg1video:
                        return "mpeg1video";
                        break;
                    case Encoders.mpeg2video:
                        return "mpeg2video";
                        break;
                    case Encoders.mpeg4:
                        return "mpeg4";
                        break;
                    case Encoders.libxvid:
                        return "libxvid";
                        break;
                    case Encoders.msmpeg4v2:
                        return "msmpeg4v2";
                        break;
                    case Encoders.msmpeg4:
                        return "msmpeg4";
                        break;
                    case Encoders.msvideo1:
                        return "msvideo1";
                        break;
                    case Encoders.png:
                        return "png";
                        break;
                    case Encoders.libtheora:
                        return "libtheora";
                        break;
                    case Encoders.libvpx:
                        return "libvpx";
                        break;
                    case Encoders.libvpx_vp9:
                        return "libvpx-vp9";
                        break;
                    case Encoders.libwebp:
                        return "libwebp";
                        break;
                    case Encoders.wmv1:
                        return "wmv1";
                        break;
                    case Encoders.wmv2:
                        return "wmv2";
                        break;
                    case Encoders.aac:
                        return "aac";
                        break;
                    case Encoders.ac3:
                        return "ac3";
                        break;
                    case Encoders.ac3_fixed:
                        return "ac3_fixed";
                        break;
                    case Encoders.alac:
                        return "alac";
                        break;
                    case Encoders.flac:
                        return "flac";
                        break;
                    case Encoders.mp2:
                        return "mp2";
                        break;
                    case Encoders.mp2fixed:
                        return "mp2fixed";
                        break;
                    case Encoders.libmp3lame:
                        return "libmp3lame";
                        break;
                    case Encoders.libopus:
                        return "libopus";
                        break;
                    case Encoders.vorbis:
                        return "vorbis";
                        break;
                    case Encoders.libvorbis:
                        return "libvorbis";
                        break;
                    case Encoders.wavpack:
                        return "wavpack";
                        break;
                    case Encoders.libwavpack:
                        return "libwavpack";
                        break;
                    case Encoders.wmav1:
                        return "wmav1";
                        break;
                    case Encoders.wmav2:
                        return "wmav2";
                        break;
                    default:
                        break;
                }
                return "";
            }
            string GetInputPortocol() { return ""; }
            string GetOutputProtocol() { return ""; }
            public override string ToString()
            {
                return string.Format("-i {0}", (GetInputPortocol() == "") ? infile : GetInputPortocol() + ":" + infile, (e == Encoders.NULL) ? "" : "-f" + this.GetEncoder(), string.Format("{0}:{1}", (GetOutputProtocol() == "") ? outfile : GetOutputProtocol() + ":" + outfile));

            }
        }
        public void RecDeskt()
        {
            Process p = new Process();
            p.StartInfo.FileName = "ffmpeg.exe";
            p.StartInfo.Arguments = " -f gdigrab -framerate 6 -i desktop out.mpg";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.OutputDataReceived += P_OutputDataReceived;
            p.ErrorDataReceived += P_ErrorDataReceived;
            p.Start();
        }
        public async void ExecuteR(FFMPEG.Command e)
        {

            Process p = new Process();
            p.StartInfo.FileName = "ffmpeg.exe";
            p.StartInfo.Arguments = e.ToString();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.OutputDataReceived += P_OutputDataReceived;
            p.ErrorDataReceived += P_ErrorDataReceived;
            p.Start();

        }

        private void P_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            File.AppendAllText("errorlog", e.Data);
        }

        private void P_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            File.AppendAllText("ffmpeg.log", e.Data);
        }
        public void ExtractFFMPEG()
        {
            File.WriteAllBytes("ffmpeg.exe", FFStatic.ffmpeg);
            File.WriteAllBytes("ffprobe.exe", FFStatic.ffprobe);
            File.WriteAllBytes("ffplay.exe", FFStatic.ffplay);
        }
    }
}
