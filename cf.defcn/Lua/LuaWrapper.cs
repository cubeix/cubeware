﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NLua;
using System.Web;
using System.Net;
namespace cf.defcn
{
    public class LuaWrapper

    {/// <summary>
     /// Create a Lua Engine
     /// </summary>
     /// <returns>Lua engine object</returns>
        public Lua Create()
        {
            var l= new NLua.Lua();
            l["WebClient"] = new System.Net.WebClient();
            //l["__LOG"] = cf.defcn.Keylogger.;
            l["IO"] = "";
            return l;
        }
        public object[] Exec(Lua Engine, string Line)
        {
            return Engine.DoString(Line);


        }
        public object[] ExecFile(Lua Engine, string Path)
        {
            return Engine.DoFile(Path);

        }
        public object[] ImportFunction(Lua Engine, string Path, params object[] parameters)
        {
            var b = Engine.LoadFile(Path);
            return b.Call(parameters);


        }
       
    }
}
